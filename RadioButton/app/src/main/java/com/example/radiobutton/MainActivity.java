package com.example.radiobutton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
 public RadioButton boy,girl;
 public TextView tv1;
 public Button bt1;
 public CheckBox cb1,cb2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //radiobutton();
        //button();
        checkbox();

    }
    public void radiobutton(){
        tv1 = findViewById(R.id.tv1);
        tv1 = findViewById(R.id.tv1);
        boy = findViewById(R.id.boy);
        girl = findViewById(R.id.girl);
        RadioGroup sex = findViewById(R.id.sex);
        sex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = findViewById(checkedId);
                tv1.setText("您点击了"+rb.getText());
            }
        });
    }
    public void checkbox(){
        tv1 = findViewById(R.id.tv1);
        bt1 = findViewById(R.id.bt1);
        cb1 = findViewById(R.id.cb1);
        cb2 = findViewById(R.id.cb2);
        cb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv1.setText("蓝求被点击");
            }
        });
        cb2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv1.setText(tv1.getText()+"\n"+"足球被点击");
            }
        });


    }
    public void button(){
        bt1 = findViewById(R.id.bt1);
        tv1 = findViewById(R.id.tv1);
        boy = findViewById(R.id.boy);
        girl = findViewById(R.id.girl);
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(boy.isChecked()){
                    tv1.setText("您点击了:"+boy.getText());
                }
                if(girl.isChecked()){
                    tv1.setText("您点击了:"+girl.getText());
                }
            }
        });
    }

}
