package com.example.imageview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
private ImageView image1;
private Button cropcenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image1 = findViewById(R.id.image1);
        cropcenter = findViewById(R.id.centercrop);
        cropcenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image1.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        });
    }


}
