package com.example.bindservice;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BinderService extends Service {
    public BinderService() {
    }
    //创建内部类
    public class MyBinder extends Binder{
        public BinderService getService(){ //创建获取Service的方法
            return BinderService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return new MyBinder();

    }
    //自定义方法，用于生成随机数
    public List getnumber(){
        List resArr = new ArrayList();
        String strNumber = "";//用于保存生成的随机数
        for(int i = 0;i < 7; i++){
            int number = new Random().nextInt(33)+1;//生成指定范围的随机整数
            if(number<10){
                strNumber="0"+String.valueOf(number);
            }else {
                strNumber=strNumber.valueOf(number);
            }
            resArr.add((strNumber));
        }
        return resArr;
    }

    @Override
    public void onDestroy() { //销毁service
        super.onDestroy();
    }
}
