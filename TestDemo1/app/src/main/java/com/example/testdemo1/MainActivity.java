package com.example.testdemo1;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private Button Button1,Button2,Button3,Button4;
    private TextView textView1,tv3;
    private RadioButton radioButton1,radioButton2;
    private  CheckBox cb1,cb2,cb3;
    private Spinner spinner1,spinner2,spinner3;
    private ListView lv1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        wa();
        ck();
        sp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.add_item:
                Toast.makeText(this,"add被点击",Toast.LENGTH_SHORT).show();
            case R.id.remove_item:
                Toast.makeText(this,"remove被点击",Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    public void initView(){
          Button1 = findViewById(R.id.Button1);
          Button1.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                 //隐式intent
                   //Intent intent = new Intent("com.example.testdemo1.START_ACTION");
                   //startActivity(intent);

                   Intent intent = new Intent();
                   //启动电话并拨打
                   intent.setAction(Intent.ACTION_CALL);
                   intent.setData(Uri.parse("tel:13765959475"));
                   startActivity(intent);
                  //启动电话不拨打
                  //intent.setAction(Intent.ACTION_DIAL);
                  //intent.setData(Uri.parse("tel:13765959475"));
                  //打开设置
                  //intent.setAction("android.settings.SETTINGS");
                  //打开联系人
                  //intent.setAction("com.android.contacts.action.LIST_CONTACTS");
                  //打开浏览器
                  //Intent intent = new Intent(Intent.ACTION_VIEW);
                  //intent.setData(Uri.parse("https://www.baidu.com"));
                  //startActivity(intent);

                  //显式intent方法
                  //Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                  //startActivity(intent);
                  //显示intent传值
                  //Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                  //intent.putExtra("data","Hi,SecondActivity");
                  //startActivity(intent);

                  //intent接收传值
//                  Intent intent = new Intent(MainActivity.this,SecondActivity.class);
//                  startActivityForResult(intent,1);
//
//                  //Toast控件显示消息
//                  Toast.makeText(MainActivity.this,"你好",Toast.LENGTH_SHORT).show();
              }
          });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        textView1 = findViewById(R.id.textView1);
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 1:
                if(requestCode == RESULT_OK){
                    String resulatData = data.getStringExtra("return");
                    //Log.d("MainActivity","wa");
                }
                break;
        }

    }
    public void wa(){
        Button Button2 = findViewById(R.id.Button2);
        Button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Toast toast =  Toast.makeText(MainActivity.this,"测试",Toast.LENGTH_SHORT);
              toast.setGravity(Gravity.CENTER,0,0);
                LinearLayout l1 = new LinearLayout(MainActivity.this);
                ImageView iv = new ImageView(MainActivity.this);
                iv.setImageResource(R.drawable.btn);
                iv.setPadding(0,0,5,0);
                l1.addView(iv);
                TextView tv = new TextView(MainActivity.this);
                tv.setText("我是通过maketext方法创建的消息提示框");
                l1.addView(tv);
                toast.setView(l1);
                toast.show();




            }
        });
    }
    public void ck(){
        final RadioButton radioButton1 = findViewById(R.id.radioButton1);
        final RadioButton radioButton2 = findViewById(R.id.radioButton2);
        Button Button4 = findViewById(R.id.Button4);
        final CheckBox cb1 = (CheckBox) findViewById(R.id.cb1);
        final CheckBox cb2 = (CheckBox)findViewById(R.id.cb2);
        final CheckBox cb3 = (CheckBox)findViewById(R.id.cb3);
        final TextView tv3 = (TextView) findViewById(R.id.tv3);
        final Spinner spinner1 = (Spinner)findViewById(R.id.spinner1);
        Button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String like = " ";
                if(radioButton1.isChecked()){
                    Toast.makeText(MainActivity.this,"男被点击",Toast.LENGTH_SHORT).show();
                    like += radioButton1.getText().toString();
                    Log.d("MainActivity","radioButton1被点击");
                }
                if(radioButton2.isChecked()){
                    Toast.makeText(MainActivity.this,"女被点击",Toast.LENGTH_SHORT).show();
                    like +=radioButton2.getText().toString();
                    Log.d("MainActivity","radioButton2被点击");}
                if (cb1.isChecked()) {
                    like += cb1.getText().toString();
                }
                if(cb2.isChecked()) {
                    like += cb2.getText().toString();
                }
                if(cb3.isChecked()) {
                    like += cb3.getText().toString();
                }

                tv3.setText("您的爱好是:" + like+spinner1.getSelectedItem().toString());

            }
        });
    }

    /**
     * Spinner方法java之代码实现
     */
    public void sp(){
        Spinner spinner2 = (Spinner)findViewById(R.id.spinner2);
        Spinner spinner3 = (Spinner)findViewById(R.id.spinner3);
        ListView lv1 = (ListView)findViewById(R.id.lv1);
        TextView tv3 = (TextView)findViewById(R.id.tv3);
        //方法1 该方法不需要去设置相应布局，选项内容可以在java代码实现
        String[] abc = new String[]{"我的","你的","他的"};
        ArrayAdapter<String>adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,abc);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);

        //方法2这个方法需要再values中设置相应的选项
        ArrayAdapter<CharSequence>adapter1 = ArrayAdapter.createFromResource(this,R.array.abc,android.R.layout.simple_dropdown_item_1line);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner3.setAdapter(adapter1);

        /**
         * ListView使用方法1SimpleAdapter
         * 支持复杂数据
         */
           //获取数据源部分
//        List<Map<String,String>> names = new ArrayList<>();
//        Map<String,String> map = new HashMap<String, String>();
//        map.put("name","x");
//        names.add(map);
//        map = new HashMap<String,String>();
//        map.put("name","2");
//        names.add(map);
//        map = new HashMap<String, String>();
//        map.put("name","3");
//        names.add(map);
//          //数据适配器部分
//        SimpleAdapter sap = new SimpleAdapter(MainActivity.this,names,android.R.layout.simple_list_item_1,new String[]{"name"},new int[]{android.R.id.text1});
//        lv1.setAdapter(sap);
        /*
         *ListView使用方法2ArrayAdapter
         * 不支持多属性数据
         */
//        String[] names = {"1","2","3"};
//        ArrayAdapter<String> adapter2 = new ArrayAdapter(MainActivity.this,android.R.layout.simple_list_item_1,android.R.id.text1,names);
//        lv1.setAdapter(adapter2);
//        String item = tv3.getText().toString();

        /*
         *
         */

    }



}
