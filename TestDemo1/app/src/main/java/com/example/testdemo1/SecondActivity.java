package com.example.testdemo1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {
  private Button Button2;
  private TextView textView2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        //接受传值
       //Intent intent = getIntent();
        //String data = intent.getStringExtra("data");
        //Log.d("SecondActivity",data);//日志
         //将接收的值用text文本显示出来
        //textView2 = findViewById(R.id.textView1);
        //textView2.setText(intent.getStringExtra("data"));

        //Button2 = findViewById(R.id.Button2);
        //Button2.setOnClickListener(new View.OnClickListener() {
            //@Override
            //public void onClick(View v) {

            //}
        //});
        init();
    }
    public void init(){
        Button2 = findViewById(R.id.Button2);
        Button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("return","Hello,MainActivity");
                setResult(RESULT_OK,intent);
                finish();
            }
        });
    }

}
