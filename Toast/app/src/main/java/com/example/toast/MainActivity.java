package com.example.toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button bt1,bt2,bt3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.bt1).setOnClickListener(this);
        findViewById(R.id.bt2).setOnClickListener(this);
        bt3 = findViewById(R.id.bt3);
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("测试");
                alertDialog.setIcon(R.drawable.ic_launcher_foreground);
                alertDialog.setMessage("只是demo");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.xiaoxi,null,false);//弹出自定义窗体，该案例是点击注册跳出注册选项框
                        Log.d("button3","记录错误");
                        PopupWindow popupWindow = new PopupWindow(view,LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT,true);
                        Log.d("button3","记录错误1");
                        popupWindow.setTouchable(true);
                        Log.d("button3","记录错误2");
                        popupWindow.showAsDropDown(bt3);
                        //popupWindow.showAtLocation(bt3,Gravity.CENTER,0,0);
                        Log.d("button3","记录错误3");
                    }
                });
                alertDialog.show();


            }
        });

    }

    @Override
    public void onClick(View v) {
        Toast toast = null;
        switch (v.getId()){
            case R.id.bt1:
                toast = Toast.makeText(this,"改变位置的Toast",Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER,100,0);//x轴方向上的偏移量，左为负，右为正，y轴方向上的偏移量，上为正，下为负
                toast.show();
                break;
            case R.id.bt2:
                toast = Toast.makeText(this,"带图片的Toast",Toast.LENGTH_LONG);
                LinearLayout linearLayout = (LinearLayout) toast.getView();//创建布局
                ImageView imageView = new ImageView(this);//创建一个图像框
                imageView.setImageResource(R.drawable.ic_launcher_background);//将图片资源加载到图像框
                linearLayout.addView(imageView);//将图像框加载到布局当中
                toast.setGravity(Gravity.CENTER,0,0);
                toast.show();
                break;
//            case R.id.bt3:
//                View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.xiaoxi,null,false);//弹出自定义窗体，该案例是点击注册跳出注册选项框
//                Log.d("button3","记录错误");
//                PopupWindow popupWindow = new PopupWindow(view,LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT,true);
//                Log.d("button3","记录错误1");
//                popupWindow.setTouchable(false);
//                Log.d("button3","记录错误2");
//                popupWindow.showAtLocation(bt3,Gravity.BOTTOM,0,0);
//                Log.d("button3","记录错误3");
//                break;
        }
    }
}
