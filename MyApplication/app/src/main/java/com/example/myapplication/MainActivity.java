package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
      private Button button4;
      private TextView textView;
      private EditText txtusername;
      private EditText txtpassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button4 = (Button) findViewById(R.id.button4);
        final ProgressBar jdt = (ProgressBar) findViewById(R.id.jdt);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton radioButton1 = (RadioButton) findViewById(R.id.radioButton1);
                RadioButton radioButton2 = (RadioButton) findViewById(R.id.radioButton2);
                final ProgressBar progressBar = (ProgressBar) findViewById(R.id.jdt);
                String Dbusername, Dbpassword;
                if (radioButton1.isChecked()) {

                    Dbusername = "Hz1234";
                    Dbpassword = "984107781";

                } else {
                    Dbusername = "Hz@qq.com";
                    Dbpassword = "984107781";

                }
                Button button4 = (Button)findViewById(R.id.button4);
                EditText txtusername = (EditText) findViewById(R.id.txtusername);
                EditText txtpassword = (EditText) findViewById(R.id.txtpassword);
                TextView txtresult = (TextView) findViewById(R.id.txtresult);
                if (txtusername.getText().toString().equals(Dbusername)) {
                    if (txtpassword.getText().toString().equals(Dbpassword)) {
                        //txtresult.setText("登录成功");
                       Toastshow("登录成功");

                                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                                startActivity(intent);


                    } else {

                        Toa("登录失败");
                    }

                } else {
                    ABC("没有该用户名");
                }
            }
        });
        final Button button3 = (Button)findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AlertDialog alert = new AlertDialog.Builder(MainActivity.this).create();//构建警告窗体
//                alert.setTitle("用户协议确认");
//                alert.setMessage("注册新用户需接受协议");
//                alert.setButton(AlertDialog.BUTTON_POSITIVE, "确认", new DialogInterface.OnClickListener() {//确认按钮点击事件
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        View contentView = LayoutInflater.from(MainActivity.this).inflate(R.layout.popup,null,false); //弹出自定义窗体，该案例是点击注册跳出注册选项框
//                        PopupWindow window = new PopupWindow(contentView,LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT,true);
//                        window.setTouchable(true);
//                        window.showAsDropDown(button3,0,0,Gravity.BOTTOM);
//                    }
//                });
//                alert.setButton(AlertDialog.BUTTON_NEGATIVE, "不同意", new DialogInterface.OnClickListener() {//否定按钮点击事件
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        Toa("只有接受协议才能注册新用户哦");
//                    }
//                });
//                alert.show();
                 final CustomDialog customDialog = new CustomDialog(MainActivity.this, new CustomDialog.OnCustomDialogListener() {
                    @Override
                    public void queding() {

                        View contentView = LayoutInflater.from(MainActivity.this).inflate(R.layout.popup,null,false); //弹出自定义窗体，该案例是点击注册跳出注册选项框
                        PopupWindow window = new PopupWindow(contentView,LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT,true);
                        window.setTouchable(true);
                        window.showAsDropDown(button3,0,0,Gravity.BOTTOM);

                    }
                });
                customDialog.show();

            }
        });

        }
    void Toastshow(String Content){     //该方法是自定义图片显示内容
        Toast toast =  Toast.makeText(MainActivity.this, Content, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER,0,0);
        LinearLayout layout = (LinearLayout) toast.getView();
        ImageView img = new ImageView(getApplicationContext());
        img.setImageResource(R.drawable.ampj);
        layout.addView(img,0);
        toast.show();
        }
     void Toa(String EX ){    //该方法是自定义显示内容，默认系统的提醒方式
         Toast toast =  Toast.makeText(MainActivity.this, EX, Toast.LENGTH_LONG);
         toast.setGravity(Gravity.CENTER,0,0);
         //LinearLayout layout = (LinearLayout) toast.getView();
         //ImageView img = new ImageView(getApplicationContext());
         //img.setImageResource(R.drawable.ampj);
         //layout.addView(img,0);
         toast.show();
        }
     void ABC(String Content){   //该方法是自定义布局显示内容
        Toast toast = Toast.makeText(MainActivity.this, Content, Toast.LENGTH_SHORT);
        View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.xiaoxi,null);
        TextView txtToastContent = (TextView)view.findViewById(R.id.txtToastContent);
        txtToastContent.setText(Content);
        toast.setView(view);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
       }
}
