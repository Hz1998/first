package com.example.myapplication;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;

public class CustomDialog extends Dialog {
    private OnCustomDialogListener custonDialogListener;
    private CheckBox cb1;
    private TextView tv1;
    private Button bt1;
    public CustomDialog(@NonNull Context context, OnCustomDialogListener customDialogListener) {
        super(context);
        this.custonDialogListener = customDialogListener;
    }
    public interface OnCustomDialogListener{
        public void queding();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);

        Window window = this.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        bt1 = findViewById(R.id.bt1);
        cb1 = findViewById(R.id.cb1);
        tv1 = findViewById(R.id.tv1);
        tv1.setMovementMethod(ScrollingMovementMethod.getInstance());
        cb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bt1.setEnabled(true);
            }
        });

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               custonDialogListener.queding();
               dismiss();
            }
        });

    }
}
