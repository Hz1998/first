package com.example.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

public class MusicService extends Service {
    static boolean isplay;//记录当前播放状态
    MediaPlayer player;//播放音乐的
    public MusicService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //创建Mediaplayer对象，并加载音频文件
        player = MediaPlayer.create(this,R.raw.beijingmusic);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(!player.isPlaying()){
            player.isPlaying();  //播放音乐
            isplay = true;  //设置状态为播放音乐
            Log.d("MainActivity","正在播放音乐");
        }
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        player.stop();
        isplay = false;
        Log.d("MainActivity","停止播放音乐");
        player.release();

    }
}
