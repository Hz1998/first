package com.example.service;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
private Button bt1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button bt1 = findViewById(R.id.bt1);
        final Intent intent = new Intent(MainActivity.this,MusicService.class);
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //启动和停止播放音乐;
                if(MusicService.isplay == false){
                  startService(intent);
                  bt1.setText("暂停");
                }else{
                   stopService(intent);
                   bt1.setText("播放");
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Button bt1 = findViewById(R.id.bt1);
        startService(new Intent(MainActivity.this,MusicService.class));
        if(MusicService.isplay == false){

            bt1.setText("暂停");
        }else{

            bt1.setText("播放");
        }
    }
}
