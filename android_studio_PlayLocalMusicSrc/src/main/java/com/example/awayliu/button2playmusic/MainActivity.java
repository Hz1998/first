package com.example.awayliu.button2playmusic;




import android.support.v7.app.AppCompatActivity;
import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends Activity {

    // 定义三个按钮
    private ImageButton mButton01, mButton02, mButton03;
    private TextView mTextView01;
    private MediaPlayer mMediaPlayer01;

    /* 声明一个Flag 判断是否暂停 */
    private boolean bIsPaused = false;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String playString = "Playing";
        final String stopString = "Stoped";

		/* ͨ关联按键 */
         mButton01 = (ImageButton) findViewById(R.id.imageButton1);
         mButton03 = (ImageButton) findViewById(R.id.imageButton3);
        //  mButton03 = (ImageButton) findViewById(R.id.myButton3);
        //  mTextView01 = (TextView) findViewById(R.id.textView);
        mTextView01 =(TextView) findViewById(R.id.textView);


        mTextView01.setText(stopString);

		/* onCreate MediaPlayer */
        mMediaPlayer01 = new MediaPlayer();

		/*  将音乐以Import的方式存储在res/raw/ */
        mMediaPlayer01 = MediaPlayer.create(MainActivity.this, R.raw.test);

            /* 创建按键监听 */
        //  public void
        mButton01.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    PlayMusic();
                    mTextView01.setText(playString);
                }catch (Exception e)
                {
                    Log.i( "Log"," Play err");
                }

            }
        });

        mButton03.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    mMediaPlayer01.stop();
                    mTextView01.setText(stopString);
                }catch (Exception e)
                {
                    Log.i( "Log"," stop err");
                }
            }
        });

    }

    public void PlayMusic(){
        Log.i("-------->","start run PlayMusic");

        try {

            if (mMediaPlayer01 != null) {
                Log.i("PlayMusic"," find source music");
                mMediaPlayer01.stop();
            }
            /*
             * 在MediaPlayer取得播放资源与stop()之后
             * 要准备Playback的状态前一定要使用MediaPlayer.prepare()
             */
            mMediaPlayer01.prepare();

            mMediaPlayer01.start();
            Log.i("PlayMusic"," startPlayMusic");

            // mTextView01.setText(R.string.str_start);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("PlayMusic"," ERR");
            mTextView01.setText(e.toString());
            e.printStackTrace();
        }

    }
}

