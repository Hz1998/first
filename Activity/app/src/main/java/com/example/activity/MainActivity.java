package com.example.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button bt1,bt2,bt3,bt4,bt5;
    private EditText et1;
    private TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast();
        Intent1();
        Intent2();
        chuangdi();
        back();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu1:
                Toast.makeText(MainActivity.this,"这是添加按钮",Toast.LENGTH_SHORT).show();
            case R.id.menu2:
                Toast.makeText(MainActivity.this,"这是移除按钮",Toast.LENGTH_SHORT).show();
        }
        return true;
    }
    public void Toast(){
        Button bt1 = findViewById(R.id.bt1);
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"Toast实例显示",Toast.LENGTH_SHORT).show();
            }
        });

    }
    public void Intent1(){
        Button bt2 = findViewById(R.id.bt2);
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                startActivity(intent);
            }
        });
    }
    public void Intent2(){
        Button bt3 = findViewById(R.id.bt3);
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("ACTION_START");
                startActivity(intent);
            }
        });
    }
    public void chuangdi(){
        Button bt4 = findViewById(R.id.bt4);
        final EditText et1 = findViewById(R.id.et1);
        bt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = et1.getText().toString();
                Intent intent = new Intent(MainActivity.this,SecondActivity.class);
                intent.putExtra("data",data);
                startActivity(intent);
            }
        });
    }
    public void back(){
        Button bt5 = findViewById(R.id.bt5);
        bt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(MainActivity.this,SecondActivity.class);
                startActivityForResult(intent1,1);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        TextView tv1 = findViewById(R.id.tv1);
        switch (requestCode){
            case 1:
                if (resultCode == 123){
                    String back = data.getStringExtra("backdata");
                    Log.d("MainActivity", back);
                    tv1.setText(back);
                }
                break;
        }

    }
}
