package com.example.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
private TextView tv1;
private Button bt1;
private EditText et1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        TextView tv1 = findViewById(R.id.tv1);

        Intent intent =getIntent();
        String data = intent.getStringExtra("data");
        tv1.setText(data);
        chuan();

    }
    public void chuan(){
        Button bt1 = findViewById(R.id.bt1);
        final EditText et1 = findViewById(R.id.et1);
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.putExtra("backdata",et1.getText().toString());
                setResult(123,intent);
                finish();
            }
        });
    }
}
